import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import { Scene, TextureLoader } from 'three'
//import typefaceFont from 'three/examples/fonts/helvetiker_regular.typeface.json'
const loadingManager = new THREE.LoadingManager()
const textureLoader = new THREE.TextureLoader(loadingManager)

import imageContacto from '../static/contacto.jpg'
import imageFilosofia from '../static/filosofia.jpg';
import imagePortfolio from '../static/portfolio.jpg';
const textureContacto = textureLoader.load(imageContacto)
const textureFilosofia = textureLoader.load(imageFilosofia);
const texturePortfolio = textureLoader.load(imagePortfolio);

const gui = new dat.GUI()
    /**
     * Base
     */
    // Canvas
const canvas = document.querySelector('canvas.webgl')


/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

// Scene
let scene = new THREE.Scene()
let sceneFilosofia = new THREE.Scene();
let sceneContacto = new THREE.Scene();
let scenePortfolio = new THREE.Scene();
/*const cube2 = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    new THREE.MeshBasicMaterial({ color: 0x00ff00 }),
);
*/

const loader = new THREE.FontLoader();

loader.load('/fonts/helvetiker_regular.typeface.json', (font) => {
    const textGeometryFilosofia = new THREE.TextBufferGeometry('Filosofia', {
        font: font,
        size: 0.2,
        height: 0.1,
        weight: 'normal',
        curveSegments: 12,
        bevelEnabled: true,
        bevelThickness: 0.03,
        bevelSize: 0.02,
        bevelOffset: 0,
        bevelSegments: 5

    })

    const textGeometryParagraph = new THREE.TextBufferGeometry(
        'La Filosofía que seguimos es siempre estar al tanto de lo último en Tecnología y aplicarla, todo esto intentando lograr un precio justo tanto como para nuestros Clientes, como para también nosotros.', {
            font: font,
            size: 0.1,
            height: 0.05,
            weight: 'normal',
            curveSegments: 12,
            bevelEnabled: true,
            bevelThickness: 0.03,
            bevelSize: 0.02,
            bevelOffset: 0,
            bevelSegments: 5
        })

    let textMaterial = new THREE.MeshBasicMaterial({ color: 0x11ff00 });

    let textoFilosofia = new THREE.Mesh(textGeometryFilosofia, textMaterial);
    sceneFilosofia.add(textoFilosofia);

    let textoFilosofiaParagraph = new THREE.Mesh(textGeometryParagraph, textMaterial);
    sceneFilosofia.add(textoFilosofiaParagraph);

    let aLight = new THREE.AmbientLight(0x404040, 5);
    sceneFilosofia.add(aLight);

});

//scene2.add(cube2);

const raycaster = new THREE.Raycaster();

window.addEventListener('dblclick', () => {
    if (!document.fullscreenElement) {
        canvas.requestFullscreen()
    } else {
        document.exitFullscreen()
    }
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 1
camera.position.y = 1
camera.position.z = 4
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Cube
 */
const cubeFilosofia = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    new THREE.MeshBasicMaterial({ map: textureFilosofia })
)

const sphereContacto = new THREE.Mesh(
    new THREE.SphereGeometry(1, 32, 32),
    new THREE.MeshBasicMaterial({ map: textureContacto })
)

const dodecahedronPortfolio = new THREE.Mesh(
    new THREE.PolyhedronGeometry(1, 0),
    new THREE.MeshBasicMaterial({ map: texturePortfolio })
)

dodecahedronPortfolio.position.set(3, 0, 0)
scene.add(dodecahedronPortfolio)
scene.add(cubeFilosofia)

sphereContacto.position.set(-3, 0, 0)
scene.add(sphereContacto)

gui.add(cubeFilosofia.position, 'y')

const renderer = new THREE.WebGLRenderer({
    canvas: canvas,
    antialias: true
})

const onClick = (event) => {
    event.preventDefault()

    const cursor = new THREE.Vector2(
        (event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1,
    );

    raycaster.setFromCamera(cursor, camera);

    const [intersection] = raycaster.intersectObject(cubeFilosofia, true);

    const [intersectionSphere] = raycaster.intersectObject(sphereContacto, true);

    const [intersectiondodeca] = raycaster.intersectObject(
        dodecahedronPortfolio,
        true,
    );


    if (intersection) {

        scene = sceneFilosofia;

    }

    if (intersectionSphere) {
        scene = sceneContacto;
    }

    if (intersectiondodeca) {
        scene = scenePortfolio;
    }
};

/**
 * Renderer
 */

renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

renderer.domElement.addEventListener('click', onClick);

window.addEventListener('resize', () => {
    // Update sizes
    sizes.width = window.innerWidth;
    sizes.height = window.innerHeight;

    // Update camera
    camera.aspect = sizes.width / sizes.height;
    camera.updateProjectionMatrix();

    // Update renderer
    renderer.setSize(sizes.width, sizes.height);
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

const clock = new THREE.Clock()
let lastElapsedTime = 0

const tick = () => {
    const elapsedTime = clock.getElapsedTime()
    const deltaTime = elapsedTime - lastElapsedTime
    lastElapsedTime = elapsedTime

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

tick()